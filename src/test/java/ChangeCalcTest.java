import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import co.coreyborders.ChangeCalc;

public class ChangeCalcTest {
	private ChangeCalc changeCalc;
	List<String> listOne = new ArrayList<String>();
	List<List<String>> lists = new ArrayList<List<String>>();
	List<String> outputLine;
	int row;
	int column;
	
	@Before
	public void setup() {
		changeCalc = new ChangeCalc();
		listOne.add(".");
		listOne.add("O");
		listOne.add(".");
		lists.add(listOne);
		lists.add(listOne);
		lists.add(listOne);
	}
	
	@Test
	public void dead_cell_in_index_0_0_with_2_living_neighbors_stays_dead() {
		row = 0;
		column = 0;
		String activeCell = ".";
		changeCalc.findNeighbors(lists, row, column);
		
		changeCalc.cellLivesOrDies(activeCell);
		
		Assert.assertEquals(".", changeCalc.getOutputLine().get(column));
		
	}
	
	@Test
	public void dead_cell_in_index_1_0_with_3_living_neighbors_comes_alive() {
		row = 1;
		column = 0;
		String activeCell = ".";
		changeCalc.findNeighbors(lists, row, column);
		
		changeCalc.cellLivesOrDies(activeCell);
		
		Assert.assertEquals("O", changeCalc.getOutputLine().get(column));
		
	}
	
	@Test
	public void dead_cell_in_index_2_0_with_2_living_neighbors_stays_dead() {
		row = 2;
		column = 0;
		String activeCell = ".";
		changeCalc.findNeighbors(lists, row, column);
		
		changeCalc.cellLivesOrDies(activeCell);
		
		Assert.assertEquals(".", changeCalc.getOutputLine().get(column));
	}
	
	@Test
	public void living_cell_in_index_0_1_with_1_living_neighbor_dies() {
		row = 0;
		column = 1;
		String activeCell = "O";
		changeCalc.findNeighbors(lists, row, column);
		
		changeCalc.cellLivesOrDies(activeCell);
		
		Assert.assertEquals(".", changeCalc.getOutputLine().get(row));
	}
	
	@Test
	public void dead_cell_in_index_0_2_with_2_living_neighbors_stays_dead() {
		row = 0;
		column = 2;
		String activeCell = ".";
		changeCalc.findNeighbors(lists, row, column);
		
		changeCalc.cellLivesOrDies(activeCell);
		
		Assert.assertEquals(".", changeCalc.getOutputLine().get(row));
	}



}
