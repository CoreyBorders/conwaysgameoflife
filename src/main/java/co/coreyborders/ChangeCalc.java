package co.coreyborders;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ChangeCalc {
	private int row = 0;
	private int column = 0;
	private int epochs = 0;
	List<String> outputLine = new ArrayList<String>();
	List<String> nextInputLine = new ArrayList<String>();
	List<List<String>> outputLists = new ArrayList<List<String>>();
	List<String> neighbors = new ArrayList<String>();
	List<String> livingNeighbors = new ArrayList<String>();
	List<List<String>> lists;

	public void findChange() {
		findEpochs();

		for (int t = 1; t <= epochs; t++) {
			getLists();

			for (int i = 0; i < lists.size(); i++) {
				nextInputLine = new ArrayList<String>();
				for (int j = 0; j < lists.get(0).size(); j++) {
					row = i;
					column = j;
					String activeCell = lists.get(i).get(j);
					findNeighbors(lists, row, column);
					cellLivesOrDies(activeCell);
				}
				outputLists.add(nextInputLine);
				System.out.println(formatString(outputLine));

				outputLine.clear();
			}

			System.out.println("===========================");
		}
	}

	private List<List<String>> getLists() {
		lists = new ArrayList<List<String>>();

		if (outputLists.isEmpty()) {
			FileReader fileReader = new FileReader();
			lists = fileReader.readFile();
		} else {
			lists = outputLists;
			outputLists = new ArrayList<List<String>>();
		}
		return lists;
	}

	public List<String> findNeighbors(List<List<String>> lists, int row, int column) {
		neighbors.clear();
		if ((row > 0 && column > 0) && (row < lists.size() - 1 && column < lists.get(0).size() - 1)) { // <--
																										// no
																										// edges
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(4);
			return neighbors;
		} else if ((row > 0 && row < lists.size() - 1) && column < 1) { // <--left
																		// column,
																		// not
																		// top
			for (int i = -1; i <= 1; i++) { // or bottom row
				for (int j = 0; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(2);
			return neighbors;
		} else if ((row > 0 && row < lists.size() - 1) && column == lists.get(0).size() - 1) { // <--
																								// right
																								// column,
																								// not
																								// top
																								// or
																								// bottom
																								// row
			for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 0; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(3);
			return neighbors;
		} else if ((row < 1) && (column > 0 && column < lists.get(0).size() - 1)) { // <--
																					// top
																					// row,
																					// not
																					// sides
			for (int i = 0; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(1);
			return neighbors;
		} else if ((row == lists.size() - 1) && (column > 0 && column < lists.get(0).size() - 1)) { // <--
																									// bottom
																									// row,
																									// not
																									// sides
			for (int i = -1; i <= 0; i++) {
				for (int j = -1; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(4);
			return neighbors;
		} else if (row < 1 && column < 1) { // <-- top left corner
			for (int i = 0; i <= 1; i++) {
				for (int j = 0; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(0);
			return neighbors;
		} else if (row < 1 && column == lists.get(0).size() - 1) { // <-- top
																	// right
																	// corner
			for (int i = 0; i <= 1; i++) {
				for (int j = -1; j <= 0; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(1);
			return neighbors;
		} else if (row == lists.size() - 1 && column == lists.get(0).size() - 1) { // <--
																					// bottom
																					// right
																					// corner
			for (int i = -1; i < 1; i++) {
				for (int j = -1; j < 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(2);
			return neighbors;
		} else { // <-- bottom left corner
			for (int i = -1; i <= 0; i++) {
				for (int j = 0; j <= 1; j++) {
					neighbors.add(lists.get(row + i).get(column + j));
				}
			}
			neighbors.remove(2);
			return neighbors;
		}

	}

	public List<String> findLivingNeighbors(List<String> neighbors) {
		livingNeighbors.clear();
		for (String neighbor : neighbors) {
			if (neighbor.equals("O")) {
				livingNeighbors.add(neighbor);
			}
		}
		return livingNeighbors;
	}

	public void cellLivesOrDies(String activeCell) {
		findLivingNeighbors(neighbors);
		if (activeCell.equals(".")) { // <-- dead cell with 3 living neighbors
										// comes to life
			if (livingNeighbors.size() == 3) {
				outputLine.add("O");
				nextInputLine.add("O");
			} else {
				outputLine.add(activeCell);
				nextInputLine.add(activeCell);
			}
		}
		if (activeCell.equals("O")) { // <-- live cell with less that 2 living
										// neighbors dies
			if (livingNeighbors.size() < 2) {
				outputLine.add(".");
				nextInputLine.add(".");
			} else if (livingNeighbors.size() > 3) { // <-- live cell with more
														// than 3
				// neighbors dies
				outputLine.add(".");
				nextInputLine.add(".");
			} else {
				outputLine.add(activeCell);
				nextInputLine.add(activeCell);
			}
		}
	}

	public int findEpochs() {
		Scanner input = new Scanner(System.in);
		System.out.println("How many epochs would you like to calculate?");
		epochs = input.nextInt();
		input.nextLine();
		return epochs;
	}

	public String formatString(List<String> outputLine) {
		String formattedOutput = outputLine.toString().replace("[", "").replaceAll("]", "").replace("[[", "")
				.replace("], [", "\n").replace("]]", "").replace(",", "").trim();
		return formattedOutput;
	}
	
	public List<String> getOutputLine() {
		return outputLine;
	}

}
