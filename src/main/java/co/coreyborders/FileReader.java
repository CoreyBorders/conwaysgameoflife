package co.coreyborders;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FileReader {
	List<List<String>> lists = new ArrayList<List<String>>();

	public List<List<String>> readFile() {
		try {
			Scanner fileInput = new Scanner(new File("input.txt"));
			System.out.println("Original From File");
			while (fileInput.hasNextLine()) {
				String line = fileInput.nextLine();
				List<String> boardLine = new ArrayList<String>(Arrays.asList(line.split("")));
				lists.add(boardLine);
				String formattedString = boardLine.toString().replace(",", "").replace("[", "").replace("]", "").trim();
				System.out.println(formattedString);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("===========================");
		return lists;

	}
}
